package com.example.user.a302cem;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

public class shop extends AppCompatActivity {

    private Button registered,sign,back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);

        Button registered = (Button)findViewById(R.id.registered);
        Button sign = (Button)findViewById(R.id.sign);
        Button back = (Button)findViewById(R.id.back);
        registered.getBackground().setAlpha(20);
        sign.getBackground().setAlpha(20);

        sign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(),signpage.class);
                startActivity(intent);
                shop.this.finish();
            }
        });

        registered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(),registeredpage.class);
                startActivity(intent);
                shop.this.finish();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(),MainActivity.class);
                startActivity(intent);
                shop.this.finish();
            }
        });

    }
}
