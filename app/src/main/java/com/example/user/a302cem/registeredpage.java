package com.example.user.a302cem;

import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import static android.provider.ContactsContract.Intents.Insert.NOTES;

public class registeredpage extends AppCompatActivity {

    private Button signback,enter;
    private EditText store,branch,a,b,c,d,atime,btime,ctime,dtime;
    private boolean isStoreValid, isSizeAValid, isSizeBValid, isSizeCValid, isSizeDValid, isPasswordValid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registeredpage);
        Button signback = (Button)findViewById(R.id.signback);
        Button enter = (Button)findViewById(R.id.enter);
        final TextInputLayout store = (TextInputLayout)findViewById(R.id.store);
        final TextInputLayout branch = (TextInputLayout)findViewById(R.id.branch);
        final TextInputLayout a = (TextInputLayout)findViewById(R.id.a);
        final TextInputLayout b = (TextInputLayout)findViewById(R.id.b);
        final TextInputLayout c = (TextInputLayout)findViewById(R.id.c);
        final TextInputLayout d = (TextInputLayout)findViewById(R.id.d);
        final TextInputLayout pwd1 = (TextInputLayout)findViewById(R.id.password1);
        final TextInputLayout pwd2 = (TextInputLayout)findViewById(R.id.password2);
        signback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goshop();
            }
        });

        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    String getstorename = store.getEditText().getText().toString().trim();
                    String getbranchname = branch.getEditText().getText().toString();
                    String getA = a.getEditText().getText().toString();
                    String getB = b.getEditText().getText().toString();
                    String getC = c.getEditText().getText().toString();
                    String getD = d.getEditText().getText().toString();
                    String getPwd1 = pwd1.getEditText().getText().toString();
                    String getPwd2 = pwd2.getEditText().getText().toString();

                    if(getstorename.isEmpty()){
                        Toast.makeText(getBaseContext(), getResources().getString(R.string.name_error), Toast.LENGTH_LONG).show();
                        isStoreValid = false;
                    }else{
                        isStoreValid = true;
                    }

                    if(getPwd1.isEmpty()){
                        Toast.makeText(getBaseContext(), getResources().getString(R.string.password_error), Toast.LENGTH_LONG).show();
                        isPasswordValid = false;
                    }else if(!getPwd2.equals(getPwd1)){
                        Toast.makeText(getBaseContext(), getResources().getString(R.string.password_notMatch), Toast.LENGTH_LONG).show();
                        isPasswordValid = false;
                    }else{
                        isPasswordValid = true;
                    }

                    if(getA.isEmpty()){
                        Toast.makeText(getBaseContext(), getResources().getString(R.string.size_error) + "A", Toast.LENGTH_LONG).show();
                        isSizeAValid = false;
                    }else{
                        isSizeAValid = true;
                    }

                    if(getB.isEmpty()){
                        Toast.makeText(getBaseContext(), getResources().getString(R.string.size_error) + "B", Toast.LENGTH_LONG).show();
                        isSizeBValid = false;
                    }else{
                        isSizeBValid = true;
                    }

                    if(getC.isEmpty()){
                        Toast.makeText(getBaseContext(), getResources().getString(R.string.size_error) + "C", Toast.LENGTH_LONG).show();
                        isSizeCValid = false;
                    }else{
                        isSizeCValid = true;
                    }

                    if(getD.isEmpty()){
                        Toast.makeText(getBaseContext(), getResources().getString(R.string.size_error) + "D", Toast.LENGTH_LONG).show();
                        isSizeDValid = false;
                    }else{
                        isSizeDValid = true;
                    }

                    if(isStoreValid && isPasswordValid && isSizeAValid & isSizeBValid & isSizeCValid & isSizeDValid){
                        //======================================
                        //put registered data
                        mysqlRegister con = new mysqlRegister();
                        con.execute(getstorename, getbranchname, getA, getB, getC, getD, getPwd1);
                        //======================================
                        Toast.makeText(getBaseContext(), "Registering", Toast.LENGTH_LONG).show();
                        goprint(getstorename,getPwd1);
                    }
                }catch (Exception e){
                    Toast.makeText(getBaseContext(), "Wrong Input", Toast.LENGTH_LONG).show();
                }
            }
        });

    }

    public void goshop(){
        Intent intent = new Intent(getBaseContext(),shop.class);
        startActivity(intent);
        registeredpage.this.finish();
    }

    public void goprint(String getstorename,String getbranchname) {
        mysqlLogin login = new mysqlLogin();
        login.execute(getstorename,getbranchname);
        Toast.makeText(getBaseContext(), "Loading...", Toast.LENGTH_LONG).show();
    }

    class mysqlLogin extends AsyncTask<String, String, String> {
        private String msg = "";
        private Connection conn;
        private String a=" ";
        private String b=" ";
        @Override
        protected String doInBackground(String... strings) {
            try {
                String storename = strings[0];
                String pwd = strings[1];

                DB_Connection_Helper dbhelper = new DB_Connection_Helper();

                String sql = "SELECT * FROM db.store WHERE name = '" + storename  + "' AND password = '" + pwd + "'";
                dbhelper.updateQuery(sql);
                ResultSet rs = dbhelper.excuteQuery(sql);
                while (rs.next()) {
                    a = rs.getInt("id")+"";
                    b = rs.getString("password");
                }

                //goshop();
            }catch(SQLException e) {
                Log.e("DB","遠端連接失敗");
                Log.e("DB", e.toString());

            }
            return msg;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            final AlertDialog.Builder normalDialog =
                    new AlertDialog.Builder(registeredpage.this);
            normalDialog.setTitle("Your Data");
            normalDialog.setMessage("Your ID is: "+a+"\nYour Password is: "+b);
            normalDialog.setPositiveButton("Yes",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            goshop();
                        }
                    });
            normalDialog.show();
            Toast.makeText(getBaseContext(), msg, Toast.LENGTH_LONG).show();
            //super.onPostExecute(s);
        }
    }
}
