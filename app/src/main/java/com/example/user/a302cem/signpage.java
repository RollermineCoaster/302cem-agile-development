package com.example.user.a302cem;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.Toast;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import static android.provider.ContactsContract.Intents.Insert.NOTES;

public class signpage extends AppCompatActivity {

    private Button enter,signback;
    private EditText inid,inpass;
    private boolean isSuccess;
    private TableLayout login_block;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signpage);

        enter = (Button)findViewById(R.id.enter);
        signback = (Button)findViewById(R.id.signback);
        inid = (EditText)findViewById(R.id.inid);
        inpass = (EditText)findViewById(R.id.inpass);
        login_block = (TableLayout)findViewById(R.id.login_block);

        login_block.getBackground().setAlpha(180);

        signback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(),shop.class);
                startActivity(intent);
                signpage.this.finish();
            }
        });

        enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String inputid = inid.getText().toString();
                String inputpass = inpass.getText().toString();


                goenter(inputid,inputpass);
            }
        });
    }
    public void goenter(final String inputid, final String inputpass){
        mysqlLogin login = new mysqlLogin();
        login.execute(inputid, inputpass);
        Toast.makeText(getBaseContext(), "Loading...", Toast.LENGTH_LONG).show();
    }

    class mysqlLogin extends AsyncTask<String, String, String> {
        private String msg = "";
        private Connection conn;
        @Override
        protected String doInBackground(String... strings) {
            try {
                String id = strings[0];
                String pwd = strings[1];

                DB_Connection_Helper dbhelper = MainActivity.helper;

                String sql = "SELECT * FROM db.store WHERE id = '" + id  + "' AND password = '" + pwd + "'";
                ResultSet rs = dbhelper.excuteQuery(sql);
                while (rs.next()) {
                    isSuccess = true;
                }

                if(isSuccess) {
                    //==============================
                    //if id pass ok
                    msg =  "Successful login!";
                    Intent intent = new Intent(getBaseContext(), shopqueue.class);
                    Bundle bundle = new Bundle();
                    bundle.putInt("storeID",Integer.valueOf(id));
                    intent.putExtras(bundle);
                    startActivity(intent);
                    signpage.this.finish();
                    //==============================
                }else{
                    //if not ok
                    msg = "Wrong ID or Password!";
                }

            }catch(SQLException e) {
                Log.e("DB","遠端連接失敗");
                Log.e("DB", e.toString());

            }
            return msg;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            Toast.makeText(getBaseContext(), msg, Toast.LENGTH_LONG).show();
            //super.onPostExecute(s);
        }
    }
}
