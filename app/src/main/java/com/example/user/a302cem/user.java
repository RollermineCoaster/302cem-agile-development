package com.example.user.a302cem;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import static android.provider.ContactsContract.Intents.Insert.NOTES;

import androidx.appcompat.app.AppCompatActivity;

public class user extends AppCompatActivity {

    private Button back,record;
    private EditText search;
    private LinearLayout searchoutput;
    private JSONObject data = new JSONObject();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        Button back = (Button)findViewById(R.id.back);
        Button record = (Button)findViewById(R.id.record);
        EditText search = (EditText)findViewById(R.id.search);
        LinearLayout searchoutput = (LinearLayout)findViewById(R.id.searchoutput);

        //add all search
        //===================================
        //this is hard code, plz put in database

        mysqlLogin login = new mysqlLogin();
        login.execute();
        Toast.makeText(getBaseContext(), "Loading...", Toast.LENGTH_LONG).show();
//        final int[] id = {1,2,3,4,5,6,7,8,9,10,11,12,13,14};
//        final String[] store = {"a shop","a shop","a shop","a shop","a shop","a shop","a shop","a shop","a shop","a shop","a shop","a shop","a shop","a shop"};
//        final String[] branch = {"1","2","3","4","5","6","7","8","9","10","11","12","13","14"};
        //===================================
        //goge(id,store,branch);

        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(),userrecord.class);
                startActivity(intent);
                user.this.finish();
            }
        });

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getBaseContext(),MainActivity.class);
                startActivity(intent);
                user.this.finish();
            }
        });

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //===============================
                //search

                ViewGroup Layout = (LinearLayout)findViewById(R.id.searchoutput);
                Layout.removeAllViews();
                mysqlsearch loadsearch = new mysqlsearch();
                loadsearch.execute(""+charSequence);
                Toast.makeText(getBaseContext(), "Loading...", Toast.LENGTH_LONG).show();
                //===============================
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    public void go(String id){
        Intent intent = new Intent(getApplicationContext(),searchdata.class);
        Bundle bundle = new Bundle();
        bundle.putString("getid",id);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    class mysqlLogin extends AsyncTask<String, String, String> {
        private String msg = "";
        private Connection conn;

        @Override
        protected String doInBackground(String... strings) {
            try {
                DB_Connection_Helper dbhelper = MainActivity.helper;

                String sql = "SELECT * FROM db.store;";

                ResultSet rs = dbhelper.excuteQuery(sql);
                String msg1 = "";
                String msg2 = "";
                String msg3 = "";
                while (rs.next()) {
                    final String a = rs.getString("id");
                    msg1 += a + ",";
                    final String b = rs.getString("name");
                    msg2 += b + ",";
                    final String c = rs.getString("branch");
                    msg3 += c + ",";
                }
                msg1 += 0+"";
                msg2 += 0+"";
                msg3 += 0+"";
                try {
                    InputStream in = openFileInput(NOTES);
                    if (in != null) {
                        InputStreamReader tmp = new InputStreamReader(in);
                        BufferedReader reader = new BufferedReader(tmp);
                        String readline = reader.readLine().toString();
                        //text.setText(data.toString());
                        tmp.close();
                        reader.close();
                        in.close();

                        data = new JSONObject(readline);
                    }
                    data.put("getid",msg1);
                    data.put("getname",msg2);
                    data.put("getbranch",msg3);
                }catch (Exception e){}
                try{
                    OutputStreamWriter out = new OutputStreamWriter(openFileOutput(NOTES,0));
                    out.write(data.toString());
                    out.close();
                }catch (IOException e){
                    Toast.makeText(getApplicationContext(), "NotOk", Toast.LENGTH_SHORT).show();
                }

            } catch (SQLException e) {
                Log.e("DB", "遠端連接失敗");
                Log.e("DB", e.toString());

            }
            return msg;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                InputStream in = openFileInput(NOTES);
                if (in != null) {
                    InputStreamReader tmp = new InputStreamReader(in);
                    BufferedReader reader = new BufferedReader(tmp);
                    String readline = reader.readLine().toString();
                    //text.setText(data.toString());
                    tmp.close();
                    reader.close();
                    in.close();

                    data = new JSONObject(readline);

                    String getids = data.getString("getid");
                    String getname = data.getString("getname");
                    String getbranch = data.getString("getbranch");

                    String ids[] = getids.split(",");
                    int id[] = new int[10000000];
                    String name[] = getname.split(",");
                    String branch[] = getbranch.split(",");

                    for (int i=0;i<ids.length;i++){
                        id[i] = Integer.parseInt(ids[i]);
                    }
//
                    LinearLayout searchoutput = (LinearLayout)findViewById(R.id.searchoutput);
                    try {
                        View.OnClickListener button_click = new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(getApplicationContext(),searchdata.class);
                                Bundle bundle = new Bundle();
                                bundle.putString("getid",v.getTag().toString());
                                intent.putExtras(bundle);
                                startActivity(intent);
                            }
                        };
                        for (int i = 0; i < name.length-1; i++) {
                            View linearLayout = findViewById(R.id.searchoutput);
                            TextView addbutton = new TextView(getBaseContext());
                            if(branch[i].equals("")){
                                addbutton.setText(name[i]);
                            }else {
                                addbutton.setText(name[i]+" ("+branch[i]+" Branch)");
                            }
                            addbutton.setId(i);
                            addbutton.setTag(id[i]);
                            addbutton.setTextColor(Color.parseColor("#000000"));
                            addbutton.setTextSize(30);
                            addbutton.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            addbutton.setOnClickListener(button_click);
                            ((LinearLayout) linearLayout).addView(addbutton);
                        }
                    }catch (Exception e){}
                }
            }
            catch (Exception rr){}
        }
    }

    class mysqlsearch extends AsyncTask<String, String, String> {
        private String msg = "";
        private Connection conn;

        @Override
        protected String doInBackground(String... strings) {
            try {
                String searchletter = strings[0];
                DB_Connection_Helper dbhelper = MainActivity.helper;

                String sql = "SELECT * FROM db.store WHERE name LIKE '%"+searchletter+"%' or branch like '%"+searchletter+"%';";

                ResultSet rs = dbhelper.excuteQuery(sql);
                String msg1 = "";
                String msg2 = "";
                String msg3 = "";
                while (rs.next()) {
                    final String a = rs.getString("id");
                    msg1 += a + ",";
                    final String b = rs.getString("name");
                    msg2 += b + ",";
                    final String c = rs.getString("branch");
                    msg3 += c + ",";
                }
                msg1 += 0+"";
                msg2 += 0+"";
                msg3 += 0+"";
                try {
                    InputStream in = openFileInput(NOTES);
                    if (in != null) {
                        InputStreamReader tmp = new InputStreamReader(in);
                        BufferedReader reader = new BufferedReader(tmp);
                        String readline = reader.readLine().toString();
                        //text.setText(data.toString());
                        tmp.close();
                        reader.close();
                        in.close();

                        data = new JSONObject(readline);
                    }
                    data.put("getid",msg1);
                    data.put("getname",msg2);
                    data.put("getbranch",msg3);
                }catch (Exception e){}
                try{
                    OutputStreamWriter out = new OutputStreamWriter(openFileOutput(NOTES,0));
                    out.write(data.toString());
                    out.close();
                }catch (IOException e){
                    Toast.makeText(getApplicationContext(), "NotOk", Toast.LENGTH_SHORT).show();
                }

            } catch (SQLException e) {
                Log.e("DB", "遠端連接失敗");
                Log.e("DB", e.toString());

            }
            return msg;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            try {
                InputStream in = openFileInput(NOTES);
                if (in != null) {
                    InputStreamReader tmp = new InputStreamReader(in);
                    BufferedReader reader = new BufferedReader(tmp);
                    String readline = reader.readLine().toString();
                    //text.setText(data.toString());
                    tmp.close();
                    reader.close();
                    in.close();

                    data = new JSONObject(readline);

                    String getids = data.getString("getid");
                    String getname = data.getString("getname");
                    String getbranch = data.getString("getbranch");

                    String ids[] = getids.split(",");
                    int id[] = new int[10000000];
                    String name[] = getname.split(",");
                    String branch[] = getbranch.split(",");

                    for (int i=0;i<ids.length;i++){
                        id[i] = Integer.parseInt(ids[i]);
                    }
//
                    LinearLayout searchoutput = (LinearLayout)findViewById(R.id.searchoutput);
                    try {
                        View.OnClickListener button_click = new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(getApplicationContext(),searchdata.class);
                                Bundle bundle = new Bundle();
                                bundle.putString("getid",v.getTag().toString());
                                intent.putExtras(bundle);
                                startActivity(intent);
                            }
                        };
                        for (int i = 0; i < name.length-1; i++) {
                            View linearLayout = findViewById(R.id.searchoutput);
                            TextView addbutton = new TextView(getBaseContext());
                            if(branch[i].equals("")){
                                addbutton.setText(name[i]);
                            }else {
                                addbutton.setText(name[i]+" ("+branch[i]+" Branch)");
                            }
                            addbutton.setId(i);
                            addbutton.setTag(id[i]);
                            addbutton.setTextColor(Color.parseColor("#000000"));
                            addbutton.setTextSize(30);
                            addbutton.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            addbutton.setOnClickListener(button_click);
                            ((LinearLayout) linearLayout).addView(addbutton);
                        }
                    }catch (Exception e){}
                }
            }
            catch (Exception rr){}
        }
    }
}

