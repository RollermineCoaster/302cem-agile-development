package com.example.user.a302cem;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.StrictMode;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.material.card.MaterialCardView;

public class MainActivity extends AppCompatActivity {

    public static DB_Connection_Helper helper;
    private MaterialCardView shop, user;
    private Button text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.ThreadPolicy p = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(p);

        text = (Button)findViewById(R.id.text);
        shop = (MaterialCardView)findViewById(R.id.shop_card);
        user = (MaterialCardView)findViewById(R.id.user_card);

        shop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goshop();
            }
        });
        user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gouser();
            }
        });
        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotext();
            }
        });

        if (helper==null){
            helper = new DB_Connection_Helper();
        }


    }

    public void goshop(){
        Intent intent = new Intent(this,shop.class);
        startActivity(intent);
        MainActivity.this.finish();
    }

    public void gouser(){
        Intent intent = new Intent(this,user.class);
        startActivity(intent);
        MainActivity.this.finish();
    }

    public void gotext(){
        Intent intent = new Intent(this,shop.class);
        startActivity(intent);
        MainActivity.this.finish();
    }
}
