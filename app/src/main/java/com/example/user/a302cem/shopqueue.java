package com.example.user.a302cem;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import static android.provider.ContactsContract.Intents.Insert.NOTES;

public class shopqueue extends AppCompatActivity {

    private DB_Connection_Helper helper = MainActivity.helper;
    private Button open,close,ain,anotin,bin,bnotin,cin,cnotin,din,dnotin,report,clearreport,reportback,refresh;
    private LinearLayout ass,re;
    private TextView getid,showa,showb,showc,showd,showa2,showb2,showc2,showd2,reportout;
    private ScrollView q;
    private Dialog Dialog;
    private JSONObject data = new JSONObject();
    private int storeID;

    //private int ainnum=0,binnum=0,cinnum=0,dinnum=0,annum=0,bnnum=0,cnnum=0,dnnum=0;
    @Override
   protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopqueue);
        Bundle bundle =this.getIntent().getExtras();
        storeID = bundle.getInt("storeID");

        open = (Button)findViewById(R.id.open);
        close = (Button)findViewById(R.id.close);
        report = (Button)findViewById(R.id.report);
        reportback = (Button)findViewById(R.id.reportback);
        clearreport = (Button)findViewById(R.id.clearreport);
        ain = (Button)findViewById(R.id.ain);
        anotin = (Button)findViewById(R.id.anotin);
        bin = (Button)findViewById(R.id.bin);
        bnotin = (Button)findViewById(R.id.bnotin);
        cin = (Button)findViewById(R.id.cin);
        cnotin = (Button)findViewById(R.id.cnotin);
        din = (Button)findViewById(R.id.din);
        dnotin = (Button)findViewById(R.id.dnotin);
        q = (ScrollView)findViewById(R.id.q);
        ass = (LinearLayout) findViewById(R.id.ass);
        re = (LinearLayout) findViewById(R.id.re);
        getid = (TextView)findViewById(R.id.getid);
        showa = (TextView)findViewById(R.id.showa);
        showd = (TextView)findViewById(R.id.showd);
        showb = (TextView)findViewById(R.id.showb);
        showc = (TextView)findViewById(R.id.showc);
        showa2 = (TextView)findViewById(R.id.showa2);
        showd2 = (TextView)findViewById(R.id.showd2);
        showb2 = (TextView)findViewById(R.id.showb2);
        showc2 = (TextView)findViewById(R.id.showc2);
        reportout = (TextView)findViewById(R.id.reportout);
        refresh = (Button)findViewById(R.id.refreshquere);

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                update();
            }
        });

        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                re.setVisibility(View.VISIBLE);
                ass.setVisibility(View.GONE);
                /*try {
                    InputStream in = openFileInput(NOTES);
                    if (in != null) {
                        InputStreamReader tmp = new InputStreamReader(in);
                        BufferedReader reader = new BufferedReader(tmp);
                        String readline = reader.readLine().toString();
                        //text.setText(data.toString());
                        tmp.close();
                        reader.close();
                        in.close();

                        data = new JSONObject(readline);
                        String out = "Group A Take a seat: "+data.getInt("ain")+"\nGroup A give up a seat: "+data.getInt("anotnum");
                        out += "\n\nGroup B Take a seat: "+data.getInt("bin")+"\nGroup B give up a seat: "+data.getInt("bnotnum");
                        out += "\n\nGroup C Take a seat: "+data.getInt("cin")+"\nGroup C give up a seat: "+data.getInt("cnotnum");
                        out += "\n\nGroup D Take a seat: "+data.getInt("din")+"\nGroup D give up a seat: "+data.getInt("dnotnum");
                        reportout.setText(out);
                    }
                }
                catch (Exception rr){}
            }*/
                ResultSet rs = helper.excuteQuery("SELECT * FROM db.time where storeid="+storeID+";");

                JSONObject group = new JSONObject();
                try {
                    group.put("A",0);
                    group.put("B",1);
                    group.put("C",2);
                    group.put("D",3);
                    group.put("0", "A");
                    group.put("1", "B");
                    group.put("2", "C");
                    group.put("3", "D");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                int total[] = new int[4];
                int in[] = new int[4];
                try{
                    while(rs.next()){
                        int temp = group.getInt(rs.getString("group"));
                        total[temp]++;
                        if (rs.getInt("in")>0){
                            in[temp]++;
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                String out = "";
                for (int i = 0; i < 4; i++){
                    try {
                        out += "\n\nGroup "+group.getString(i+"")+" Take a seat: "+in[i]+"\nGroup "+group.getString(i+"")+" give up a seat: "+(total[i]-in[i]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                reportout.setText(out);
            }
        });

        reportback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                re.setVisibility(View.GONE);
                ass.setVisibility(View.VISIBLE);
            }
        });

        clearreport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder normalDialog =
                        new AlertDialog.Builder(shopqueue.this);
                normalDialog.setTitle("Want to Clear?");
                normalDialog.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                /*try {
                                    data.put("ain",0);
                                    data.put("anotnum",0);
                                    data.put("bin",0);
                                    data.put("bnotnum",0);
                                    data.put("cin",0);
                                    data.put("cnotnum",0);
                                    data.put("din",0);
                                    data.put("dnotnum",0);
                                }catch (Exception e){}
                                try{
                                    OutputStreamWriter out = new OutputStreamWriter(openFileOutput(NOTES,0));
                                    out.write(data.toString());
                                    out.close();
                                }catch (IOException e){
                                    Toast.makeText(getApplicationContext(), "NotOk", Toast.LENGTH_SHORT).show();
                                }*/
                                helper.updateQuery("delete from db.time where storeid="+storeID+";");
                                re.setVisibility(View.GONE);
                                ass.setVisibility(View.VISIBLE);
                                //getid.setText(data.toString());
                            }
                        });
                normalDialog.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                normalDialog.show();
            }
        });

        open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //======================================
                //set databace a-d to 0
                helper.updateQuery("insert into db.query (storeid) values ("+storeID+");");
                update();
                //ainnum=binnum=cinnum=dinnum=annum=bnnum=cnnum=dnnum=0;
                //======================================
                q.setVisibility(View.VISIBLE);
                ass.setVisibility(View.GONE);
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder normalDialog =
                        new AlertDialog.Builder(shopqueue.this);
                normalDialog.setTitle("Want to Close?");
                normalDialog.setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                /*try {
                                    InputStream in = openFileInput(NOTES);
                                    if (in != null) {
                                        InputStreamReader tmp = new InputStreamReader(in);
                                        BufferedReader reader = new BufferedReader(tmp);
                                        String readline = reader.readLine().toString();
                                        //text.setText(data.toString());
                                        tmp.close();
                                        reader.close();
                                        in.close();

                                        data = new JSONObject(readline);
                                        ainnum += data.getInt("ain");
                                        annum += data.getInt("anotnum");
                                        binnum += data.getInt("bin");
                                        bnnum += data.getInt("bnotnum");
                                        cinnum += data.getInt("cin");
                                        cnnum += data.getInt("cnotnum");
                                        dinnum += data.getInt("din");
                                        dnnum += data.getInt("dnotnum");
                                    }
                                }
                                catch (Exception rr){}

                                try {
                                    data.put("ain",ainnum);
                                    data.put("anotnum",annum);
                                    data.put("bin",binnum);
                                    data.put("bnotnum",bnnum);
                                    data.put("cin",cinnum);
                                    data.put("cnotnum",cnnum);
                                    data.put("din",dinnum);
                                    data.put("dnotnum",dnnum);
                                }catch (Exception e){}
                                try{
                                    OutputStreamWriter out = new OutputStreamWriter(openFileOutput(NOTES,0));
                                    out.write(data.toString());
                                    out.close();
                                }catch (IOException e){
                                        Toast.makeText(getApplicationContext(), "NotOk", Toast.LENGTH_SHORT).show();
                                }*/
                                //getid.setText(data.toString());
                                helper.updateQuery("delete from db.query where storeid="+storeID+";");
                                q.setVisibility(View.GONE);
                                ass.setVisibility(View.VISIBLE);
                            }
                        });
                normalDialog.setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                normalDialog.show();
            }
        });

        ain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //ainnum++;
                //showa.setText("A"+(ainnum+annum+1));
                in("A",true);
                update();
            }
        });

        anotin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //annum++;
                //showa.setText("A"+(ainnum+annum+1));
                in("A",false);
                update();
            }
        });

        bin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //binnum++;
                //showb.setText("B"+(binnum+bnnum+1));
                in("B",true);
                update();
            }
        });

        bnotin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //bnnum++;
                //showb.setText("B"+(binnum+bnnum+1));
                in("B",false);
                update();
            }
        });

        cin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //cinnum++;
                //showc.setText("C"+(cinnum+cnnum+1));
                in("C",true);
                update();
            }
        });

        cnotin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //cnnum++;
                //showc.setText("C"+(cinnum+cnnum+1));
                in("C",false);
                update();
            }
        });

        din.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //dinnum++;
                //showd.setText("D"+(dinnum+dnnum+1));
                in("D",true);
                update();
            }
        });

        dnotin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //dnnum++;
                //showd.setText("D"+(dinnum+dnnum+1));
                in("D",false);
                update();
            }
        });
    }

    public int in(String group, boolean wasIn){
        ResultSet rs = helper.excuteQuery("SELECT * FROM db.query where storeid="+storeID+";");
        int current = 0;
        int last = 0;
        try {
            rs.next();
            current = rs.getInt("current"+group);
            last = rs.getInt("last"+group);
            if (current < last){
                current++;
                helper.updateQuery("update db.query set current"+group+"="+current+" where storeid="+storeID+";");
                helper.updateQuery("insert into db.time (`storeid`, `group`, `in`, `waiting`) values ("+storeID+", '"+group+"', "+wasIn+", "+(last-current)+");");
            }else{
                Toast.makeText(getBaseContext(), "No one waiting!", Toast.LENGTH_SHORT).show();
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    public void update(){
        ResultSet rs = helper.excuteQuery("SELECT * FROM db.query where storeid="+storeID+";");
        try {
            rs.next();
            showa.setText("A"+rs.getInt("currentA"));
            showb.setText("B"+rs.getInt("currentB"));
            showc.setText("C"+rs.getInt("currentC"));
            showd.setText("D"+rs.getInt("currentD"));
            showa2.setText("A"+rs.getInt("lastA"));
            showb2.setText("B"+rs.getInt("lastB"));
            showc2.setText("C"+rs.getInt("lastC"));
            showd2.setText("D"+rs.getInt("lastD"));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
