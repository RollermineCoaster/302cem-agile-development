package com.example.user.a302cem;

import android.content.DialogInterface;
import android.content.Intent;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import static android.provider.ContactsContract.Intents.Insert.NOTES;

public class searchdata extends AppCompatActivity {

    private DB_Connection_Helper helper = MainActivity.helper;
    private Button back, refresh;
    private TextView ticket;
    private String[] groups = {"A", "B", "C", "D"};
    private Button[] gBtn = new Button[groups.length];
    private String[] sizes = new String[groups.length];
    private int[] currents = new int[groups.length];
    private int[] lasts = new int[groups.length];
    private String searchid;
    private int waitnum;
    private String shopName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchdata);

        back = (Button)findViewById(R.id.back);
        refresh = (Button)findViewById(R.id.refresh);
        ticket = (TextView)findViewById(R.id.ticket);
        gBtn[0] = (Button)findViewById(R.id.a);
        gBtn[1] = (Button)findViewById(R.id.b);
        gBtn[2] = (Button)findViewById(R.id.c);
        gBtn[3] = (Button)findViewById(R.id.d);
        gBtn[0].getBackground().setAlpha(20);
        gBtn[1].getBackground().setAlpha(20);
        gBtn[2].getBackground().setAlpha(20);
        gBtn[3].getBackground().setAlpha(20);

        Bundle bundle =this.getIntent().getExtras();
        searchid = bundle.getString("getid");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),user.class);
                startActivity(intent);
                searchdata.this.finish();
            }
        });
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                update();
            }
        });
        update();
    }
    public void add(final int groupID){
        final AlertDialog.Builder normalDialog =
                new AlertDialog.Builder(searchdata.this);
        normalDialog.setTitle("Group "+groups[groupID]);
        //=============================
        //get last waiting number of group

        waitnum = lasts[groupID]+1;
        //=============================
        normalDialog.setMessage("Accept this number? "+groups[groupID]+waitnum);
        normalDialog.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //=======================
                        //add to record
                        try {
                            JSONObject data = new JSONObject();
                            data.put("shop", searchid);
                            data.put("shopName", shopName);
                            data.put("groupID", groupID);
                            data.put("num", waitnum);
                            OutputStreamWriter out = new OutputStreamWriter(openFileOutput("ticket",0));
                            out.write(data.toString());
                            out.close();
                            helper.updateQuery("update db.query set last"+groups[groupID]+"="+waitnum+" where storeid="+searchid+";");
                            ticket.setText(groups[groupID]+waitnum+"");
                            update();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        //=======================
                    }
                });
        normalDialog.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        normalDialog.show();
    }

    public void update(){

        //========================
        //use id find queue
        try {
            ResultSet rs = helper.excuteQuery("SELECT * FROM db.store where id="+searchid+";");
            rs.next();
            shopName = rs.getString("name")+" ("+rs.getString("Branch")+" Branch)";
            for (int i = 0; i < groups.length; i++){
                sizes[i] = rs.getString("size"+groups[i]);
            }
            rs = helper.excuteQuery("SELECT * FROM db.query where storeid="+searchid+";");
            rs.next();
            for (int i = 0; i < groups.length; i++){
                currents[i] = rs.getInt("current"+groups[i]);
                lasts[i] = rs.getInt("last"+groups[i]);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (int i = 0; i < groups.length; i++){
            String temp = groups[i]+" "+sizes[i]+"\nCurrent: "+currents[i]+"\nLast: "+lasts[i];
            gBtn[i].setText(temp);
        }
        //========================
        try {
            InputStream in = openFileInput("ticket");
            if (in != null) {
                InputStreamReader tmp = new InputStreamReader(in);
                BufferedReader reader = new BufferedReader(tmp);
                String readline = reader.readLine().toString();
                //text.setText(data.toString());
                tmp.close();
                reader.close();
                in.close();
                JSONObject data = new JSONObject(readline);
                if (searchid.equals(data.getInt("shop")+"")){
                    int temp1 = data.getInt("groupID");
                    int temp2 = data.getInt("num");
                    if (temp2 >= currents[temp1]){
                        ticket.setText(groups[temp1]+temp2+"");
                    }
                }
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        if (ticket.getText().equals("")){
            gBtn[0].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    add(0);
                }
            });
            gBtn[1].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    add(1);
                }
            });
            gBtn[2].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    add(2);
                }
            });
            gBtn[3].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    add(3);
                }
            });
        }else{
            for (int i = 0; i < groups.length; i++){
                gBtn[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "You already got a ticket!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }
}
