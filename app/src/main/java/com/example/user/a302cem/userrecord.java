package com.example.user.a302cem;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class userrecord extends AppCompatActivity {

    private TextView shopName, ticket;
    private Button back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userrecord);
        shopName = findViewById(R.id.shop_name);
        ticket = findViewById(R.id.ticket_record);
        back = findViewById(R.id.button);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(),user.class);
                startActivity(intent);
                userrecord.this.finish();
            }
        });
        String[] group = {"A","B","C","D"};
        try {
            InputStream in = openFileInput("ticket");
            if (in != null) {
                InputStreamReader tmp = new InputStreamReader(in);
                BufferedReader reader = new BufferedReader(tmp);
                String readline = reader.readLine();
                tmp.close();
                reader.close();
                in.close();
                JSONObject data = new JSONObject(readline);
                if (data != null){
                    shopName.setText(data.getString("shopName"));
                    ticket.setText(group[data.getInt("groupID")]+data.getString("num"));
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
