package com.example.user.a302cem;

import android.util.Log;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DB_Connection_Helper {
    private final String endpoint = "database-1.cfnsngbgz8fk.us-east-2.rds.amazonaws.com";
    private final String user = "admin";
    private final String password = "ZiEdkGVCPrU8ynAUlqgk";
    private final int port = 3306;
    private final String db = "db";
    private final String driver = "com.mysql.jdbc.Driver";
    private final String url = "jdbc:mysql://"+endpoint+":"+port+"/"+db;
    private Connection connection;
    private Statement statement;
    private ResultSet resultSet;

    public DB_Connection_Helper() {
        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(url, user, password);
            statement = connection.createStatement();
            Log.i("my_log", "database connected");
        } catch (ClassNotFoundException e) {
            Log.e("my_error", "driver not found");
        } catch (SQLException e) {
            Log.e("my_error", "get connection or createStatement error");
            e.printStackTrace();
        }
    }

    public ResultSet excuteQuery(String query) {
        try {
            resultSet = statement.executeQuery(query);
            Log.i("my_log", "query executed");
        } catch (SQLException e) {
            Log.e("my_error", "excute query error");
            e.printStackTrace();
        } finally {
            return resultSet;
        }
    }

    public int updateQuery(String query) {
        int effectedRow = 0;
        try {
             effectedRow = statement.executeUpdate(query);
             Log.i("my_log", "query updated");
        } catch (SQLException e) {
            Log.e("my_error", "update query error");
            e.printStackTrace();
        } finally {
            return effectedRow;
        }
    }
}
